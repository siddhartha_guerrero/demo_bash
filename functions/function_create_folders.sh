function create_ins_folders 
{
    param_ins=$1
    folder=$2
    echo "creating install folders"
    mkdir -p $folder/$param_ins/bin $folder/$param_ins/etc $folder/$param_ins/usr
        if [ "$?" = "0" ]; then
            echo "install folders was created"
            else
            echo "There is an error creating directories!"
            exit 2
        fi
    echo "$folder/$param_ins" > variables/temp_var 
    echo "ins_path=$folder" > variables/$param_ins
}