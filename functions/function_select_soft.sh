# This script allows you install software with user interaction

source functions/function_manage_soft.sh
$folder=install
function soft_available
{
    local PS3='Please enter your choice or press enter to show the menu: '
    local options=("SAS" "DS" "SQ" "LN" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "SAS")
                param_ins=SAS
                man_soft $param_ins $folder
                ;;
            "DS")
                param_ins=DS
                man_soft $param_ins $folder
                ;;
            "SQ")
                param_ins=SQ
                man_soft $param_ins $folder
                ;;
            "LN")
                param_ins=LN
                man_soft $param_ins $folder
                ;;
            "Quit")
                break
                ;;
            *) echo invalid option;;
        esac
    done    
}