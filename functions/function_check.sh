param_ins=$1
source functions/function_create_folders.sh
source functions/function_remove.sh

function check_prev_inst
{
	if [ -r variables/$param_ins ]; then
	echo -e "WARNING!!! Previous Install of $param_ins was found"
	read -r -p "Do you want remove the previous install? [y/N] " response
        if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
        then
            
            remove_dir $param_ins $folder  2> logs/error.log
                if [ "$?" = "0" ]; then
                    echo "$param_ins was remove"
                else
                    echo "There is an error removing directories check the logs!"
        fi
            
        else
            echo "Please check that your software is correct and try again"    
        fi
	fi
}


function check_install_exist
{
    if [ -d $folder ]
    then
        if [ -d $folder/$param_ins ]
        then
        echo "The folder already exist please select remove before to install again"
        fi
    else
    echo "Please enter the name of the folder"
    read folder
    create_ins_folders $param_ins $folder
    fi
}