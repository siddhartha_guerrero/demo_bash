#This is the main menu this menu manage all the thing in the main script 
#and i made this comment because the files was coded ASNI instead of UTF-8

source variables/auto.conf
source functions/function_select_soft.sh

function start_menu
{
    
    local PS3='Please enter your choice or press enter to show the menu: '
    local options=("Manage Software" "Work with data" "Backup" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "Manage Software")
                if [ $enable="no" ]; then
                    soft_available $param_ins $folder
                else 
                    silent_mode
                    echo "The installation was $install_status press enter to return"
					read
                fi
                ;;
            "Work with data")
                clear
                echo "This function is not available right now"
                ;;
            "Backup")
                clear
                echo "This function is not available right now"
                ;;
            "Quit")
                break
                ;;
			*) echo invalid option;;
        esac
    done    
}