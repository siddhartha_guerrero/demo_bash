# This script allows Install and remove installed software

source functions/function_check.sh
source functions/function_remove.sh
source functions/function_create_file.sh
param_ins=$1
folder=$2

function man_soft
{
    echo "You are Managing $param_ins software"
    local PS3='Please enter your choice or press enter to show the menu: '
    local options=("Prepare" "Install" "Remove" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "Prepare")
                check_install_exist $param_ins $folder
                ;;
            "Install")
                create_ins_files $param_ins $folder
                ;;
            "Remove")
                remove_dir $param_ins $folder
                ;;
            "Quit")
                break
                ;;
            *) echo invalid option;;
        esac
    done
}

