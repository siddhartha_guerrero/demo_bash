#This code gets 

source variables/install_var.conf
function check_install
{
    default_status="no"
    echo -e "\e[39mChecking for installed software"
    if [ $default_status == $SAS_INSTALL ]; then
    echo -e "\e[39mSAS Status            \e[91mNot Installed"
    else 
    echo -e "\e[39mSAS Status            \e[92mInstalled"
    fi
    if [ $default_status == $DS_INSTALL ]; then
    echo -e "\e[39mDataStage Status      \e[91mNot Installed"
    else 
    echo -e "\e[39mDataStage Status      \e[92mInstalled"
    fi
    if [ $default_status == $SQ_INSTALL ]; then
    echo -e "\e[39mSeaQuest Status       \e[91mNot Installed"
    else 
    echo -e "\e[39mSeaQuest Status       \e[92mInstalled"
    fi
    if [ $default_status == $LN_INSTALL ]; then
    echo -e "\e[39mLimeNetwork Status    \e[91mNot Installed"
    else 
    echo -e "\e[39mLimeNetwork Status    \e[92mInstalled"
    fi
}